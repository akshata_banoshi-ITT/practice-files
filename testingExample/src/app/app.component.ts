import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Testing Example';
  public exampleString:string[]=[];
  constructor(){
    this.exampleString.push("Hello, It's Akshata here.");
  }
  ngOnInit(){
    this.exampleString.push("ngOnInit Function");
  }
  Add(a:number,b:number):number{
    return a+b;
  }
}
